// @File(label = "Input directory", style = "directory") input
// @String(label = "Kernel Size",description="Kernel size in pixels for comparing variance. The image will be cropped to a multiple of this number",value=10,persist=false) kernel
// @Boolean(label = "Show heatmap?",description="Shows from which slice each block is taken.",value=false) heatmap
// @Boolean(label = "Verbose Mode?",description="Prints progress to the log window.",value=false,persist=false) verbose

//---------------------------------------------------------------------------------------
//  Fusing parts of an image stack based on pixel variance
//---------------------------------------------------------------------------------------
//
//												Written by Dave Mason [dnmason@liv.ac.uk]
//												Liverpool Centre for Cell Imaging
//												http://cci.liv.ac.uk
//
//												Provided under an MIT licence (see LICENCE file)

setBatchMode(true);
close("*");

run("Image Sequence...", "open=["+input+"] sort");
//run("Images to Stack", "name=Stack title=[] use");
rename("Stack");

//-- record the number of slices in the image
s=nSlices;
if (verbose==true){print("Number of slices in stack: "+s);}

//-- Crop the image so it's an even 10px dimension
w1=getWidth;
h1=getHeight;
w2=floor(w1/kernel)*kernel;
h2=floor(h1/kernel)*kernel;
if (w1!=w2||h1!=h2){
if (verbose==true){
	print("Adjusting Width: "+w1+" -> "+w2);
	print("Adjusting Height: "+h1+" -> "+h2);
	}
run("Canvas Size...", "width="+w2+" height="+h2+" position=Top-Left");
}

//-- Make an output image
newImage("Output", "8-bit black", w2, h2, 1);

//-- Optionally make a Heatmap_blk of xBlocks by yBlocks then set LUT and scale by the kernel factor later
if (heatmap==true){newImage("Heatmap_blk", "8-bit black", w2/kernel, h2/kernel, 1);}

selectWindow("Stack");

//-- for each block, find the best focus (highest StDev) and use to create the final image
cols=(w2/kernel); //-- X
rows=(h2/kernel); //-- Y
if (verbose==true){print("Images made up of "+cols+" x "+rows+" blocks");}
blocks=rows*cols;
if (verbose==true){print("Total blocks =  "+blocks);}
for (blknum=0;blknum<blocks;blknum++){

//-- calculate position here remember that LINEARINDEX = X + (Y * [ELEMENTS IN X])
//-- Convert to 2D indexing
x=blknum%cols;
y=(blknum-x)/cols;
if (verbose==true){print("Processing block : "+blknum+"/"+blocks+" (x="+x+",y="+y+")");}
out=newArray(s);

for (i=0;i<s;i++){
	selectWindow("Stack");
	run("Specify...", "width="+kernel+" height="+kernel+" x="+(x*kernel)+" y="+(y*kernel)+" slice="+(i+1));	
	//setSlice(i+1);
	List.clear();
	List.setMeasurements;
	out[i] = List.get("StdDev");
} //-- Slice loop

	rankout=Array.reverse(Array.rankPositions(out));
	//rankout=Array.reverse(rankout);
	//-- rankout[0] now contains the slice-1 of highest StDev
	//-- Switch to output and copy the corresponding block
	run("Specify...", "width="+kernel+" height="+kernel+" x="+(x*kernel)+" y="+(y*kernel)+" slice="+(rankout[0]+1));
	run("Copy");
	selectWindow("Output");
	makeRectangle(x*kernel,y*kernel,kernel,kernel);
	run("Paste");
	run("Select None");

	//-- Update Heatmap_blk if requested
	if (heatmap==true){
		selectWindow("Heatmap_blk");
		setPixel(x,y,rankout[0]+1);
		selectWindow("Stack");
		}

} //-- Block loop
close("Stack");

//-- Tidy up heatmap if requested
if (heatmap==true){
selectWindow("Heatmap_blk");
run("Scale...", "x="+kernel+" y="+kernel+" interpolation=None create title=Heatmap_px");
close("Heatmap_blk");
selectWindow("Heatmap_px");
run("16 colors");
setMinAndMax(1, s);
}
setBatchMode("exit and display");